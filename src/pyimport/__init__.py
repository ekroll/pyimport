import subprocess
import importlib
import sys


def pyimport(module):
    try:
        return importlib.import_module(module)
    except:
        pip = [sys.executable, "-m", "pip", "install", module]
        print(f"pip command = {pip}")
        if subprocess.run(pip, shell=True) != 0: sys.exit()
        return importlib.import_module(module)
