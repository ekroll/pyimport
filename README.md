# Lazy Python Import
This is a small module that let you import pip packages directly. \
`python3 -m pip install git+https://gitlab.com/ekroll/pyimport.git`


```python
# Example
from pyimport import pyimport
tqdm = pyimport("tqdm")
```
